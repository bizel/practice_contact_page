var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var nodeMailer = require("nodemailer");


var app = express();


app.set('views', './views');
app.set('view engine','jade');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join('contact-page','public')));


//create a route
app.get('/',function (req,res) {
  res.render('index',{title:'Welcome'});
});

//create a route
app.get('/about', function(req,res){
  res.render('about', {title:'About'});
});

//route to get the contact form
app.get('/contact', function(req,res){
  res.render('contact',{title:'Contact'});
});

app.post('/contact/send', function (req, res){
  var transporter = nodeMailer.createTransport({
    service:'Gmail',
    secure: false,
    port:25,
    auth:
    {
      user:'yourEmail@Address',
      pass:'yourPassword'
    },
    tls:{
      rejectUnauthorized:false
    }
  });

  var mailOptions =
  {
    from:'Persons Name <personsEmail@Adrdress>',
    to: 'yourEmail@Address',
    subject: 'Test',
    text: 'Testing of NodeMailer on Node.js contact form. This is from Name: '+ req.body.name+' Email:'+req.body.email+' Message:'+req.body.message,
    html:'<p><ul><li>this a a simple test from Name:'+ req.body.name+'</li><li> Email:'+req.body.email+'</li><li>Message:'+req.body.message+'</li></ul>',
  };

  transporter.sendMail(mailOptions, function (err,info)
  {
    if(err)
    {
      console.log(err);
      res.redirect('/');
    }else
    {
      console.log('Message send');
      res.redirect('/');
    }
  });

});

app.listen(3000, function() {
    console.log("App listening on port 3000!");
});
